pyv4l2jpeg
=======

Python bindings for Video4Linux2 API using linux v4l2 library.
This repository is based on https://github.com/pupil-labs/pyv4l2.

Provides
* Full access to all uvc settings (Zoom,Focus,Brightness,etc.)
* Full access to all stream and format parameters (rates,sizes,etc.)
* Enumerate all capture devices with list_devices()
* Capture instance will always grab mjpeg conpressed frames from cameras.

Image data is returned as bytes object of jpeg image.

