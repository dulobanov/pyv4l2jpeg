from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

extensions = [
    Extension(
        name="pyv4l2jpeg",
        sources=['pyv4l2jpeg.pyx'],
        libraries=['v4l2', 'rt'],
    ),
]

setup(
    name="pyv4l2jpeg",
    version="0.1.2",  # make sure this is the same in pyv4l2jpeg.pxy
    description="V4L2 bindings for JPEG format only.",
    ext_modules=cythonize(extensions)
)
